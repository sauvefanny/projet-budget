# PROJET APPLICATION BUDGET

### Réalisation

Il va falloir créer une tranche complète de la base de donnée jusqu'à l'application. Nous aurons donc 2 applications, le serveur node.js et le front en React.

### Organisation

## 1/Faire un diagramme de classe représentant les entités qui persisteront:

<img src="./image/diagrammeDeClasse.png" width="auto" height="300" />

## 2/Créer une script SQL avec création de la table et insertion de quelques données de tests

- Avant de créer une table, il faut définir ses colonnes, le type de chacune des colonnes et décider si elles peuvent ou non contenir NULL  
- Chaque table créée doit définir une clé primaire.
- j'ai pris la descision de nommer en minuscule sans accent, séparez par un _ et au singulier = COHERENCE


```
DROP TABLE IF EXISTS Gestion_de_budget;
CREATE TABLE `Gestion_de_budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(256) DEFAULT NULL,
  `date_depense` date NOT NULL,
  `montant_depense` double NOT NULL,
  `categorie` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

```
