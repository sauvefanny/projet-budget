/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET SQL_NOTES=0 */;
DROP TABLE IF EXISTS budget;
CREATE TABLE `budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(300) NOT NULL,
  `titre` varchar(256) DEFAULT NULL,
  `date_depense` date NOT NULL,
  `montant_depense` double NOT NULL,
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
INSERT INTO budget(id,titre,date_depense,montant_depense,categorie) 
VALUES(1,'achat de vêtements','2021-05-18',35.5,'vetement'),(2,'pique-nique','2021-05-18',55,'alimentation'),(3,'marché','2021-06-20',21.5,'alimentation'),(4,'lessive','2021-05-18',10,'hygiène'),(5,'10 repas ','2021-05-18',35.5,'alimentation'),(6,'biocoop','2021-05-18',82.35,'alimentation'),(7,'short Léana','2021-05-18',5,'vetement');