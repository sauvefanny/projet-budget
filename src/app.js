import "dotenv-flow/config";
import {server} from "./server";

const port = process.env.PORT || 8000;

server.listen(port, () => {
    console.log('listening http://localhost:'+ port);  
})

//Ces deux trucs permettent de solutionner le soucis de address already in use à priori
process.on('SIGINT', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});

process.on('exit', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});
