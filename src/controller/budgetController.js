import { Router } from "express";
import { BudgetRepository } from '../repository/budgetRepo';

export const budgetController = Router();

budgetController.get("/all", async (req, res) => {
    try {
        let data = await new BudgetRepository().findAll();
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
});

///////
budgetController.get('/:id', async (req, res) => {
    try {
        let data = await new BudgetRepository().findById(req.params.id);
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
});

/////

budgetController.post('/', async (req, res) => {
    try {
        await new BudgetRepository().add(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
});

//update
budgetController.put('/', async (req, res) => {
    try {
        await new BudgetRepository().update(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
});


budgetController.delete('/:id', async (req, res) => {
    try {
        await new BudgetRepository().delete(req.params.id);
        res.end();
    } catch (error) {
        res.status(500).json({
            message: 'Server Error'
        })
    }
});